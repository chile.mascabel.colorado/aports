# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet-pam
pkgver=5.21.2
pkgrel=0
pkgdesc="KWallet PAM integration"
# armhf blocked by extra-cmake-modules
# s390x blocked by kwallet
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	kwallet-dev
	libgcrypt-dev
	linux-pam-dev
	socat
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwallet-pam-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="0b841dd93022efd06c32f1a0527d613c45e038468260b6713f27a5aeaf6b4c463864a982b263333783df8751948b72299c72eb8bca77992f2b332910d9953de9  kwallet-pam-5.21.2.tar.xz"
