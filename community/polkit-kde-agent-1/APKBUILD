# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=polkit-kde-agent-1
pkgver=5.21.2
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
# armhf blocked by extra-cmake-modules
# mips, mips64, s390x blocked by polkit-qt-1
arch="all !armhf !s390x !mips !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="polkit-elogind"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/polkit-kde-agent-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="7c2b8b2be739044283d85c3cc79fe0910cf87da4e897f43e644727679e81fbf7e5824855d6a6019e304b840894d390a85c81b4f76fe122064e4fd4dc7f1430fe  polkit-kde-agent-1-5.21.2.tar.xz"
