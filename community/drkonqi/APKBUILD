# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=drkonqi
pkgver=5.21.2
pkgrel=0
pkgdesc="The KDE crash handler"
# armhf blocked by extra-cmake-modules
# s390x blocked by kconfigwidgets
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kwallet-dev
	kwidgetsaddons-dev
	kxmlrpcclient-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	syntax-highlighting-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/drkonqi-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=OFF # Broken
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="2870ac6305200eb7085bafc3e966759a901192e9214c724cb2b33eb202aaeb0cc251819ba290a09df1acf8726dbd210d52e76c1ad4c2537678a73f7d9e9b032c  drkonqi-5.21.2.tar.xz"
