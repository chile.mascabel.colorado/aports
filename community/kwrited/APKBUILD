# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwrited
pkgver=5.21.2
pkgrel=0
pkgdesc="KDE daemon listening for wall and write messages"
arch="all !armhf" # qt5-qtdeclarative-dev  unavilable on armhf
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	knotifications-dev
	kpty-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwrited-$pkgver.tar.xz"
options="!check" # No tests available

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="3636d5eb5fdf0ddea9ac91a28c9302a0c090a825821540ae6592dc1f08483fb4f16705c9a0401c0378bc7b77c1f4d858af9501d1971b7419ff25c53e0a5f585e  kwrited-5.21.2.tar.xz"
